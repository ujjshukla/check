import arrowAI.main as ArrowAI
# import arrowAI.model as ArrowModel
# import arrowAI.prediction as ArrowAIPrediction

def train():

	dataset = ArrowAI.load_data('./data/iris.csv')
	cluster = ArrowAI.createCluster(dataset, {"name": "my cluster", "k": 5, "model_clusters": True})
	global model1
	model1 = ArrowAI.createModel(cluster, {"name": "model for centroid 000001", "centroid": "000001"})
	
def predict(data):
	global model1
	prediction1 = ArrowAI.predict(model1,{"sepal length": 5,"sepal width": 2.5},{"name": "my prediction1"})
	return ArrowAI.pprint(prediction1)

# model2 = ArrowModel.createModel(cluster, {"name": "model for centroid 000001", "centroid": "000002"})
# prediction2 = ArrowAI.predict(model2,{"sepal length": 5,"sepal width": 2.5},{"name": "my prediction2"})
# ArrowAI.pprint(prediction2)

# model3 = ArrowModel.createModel(cluster, {"name": "model for centroid 000001", "centroid": "000003"})
# prediction3 = ArrowAI.predict(model3,{"sepal length": 5,"sepal width": 2.5},{"name": "my prediction3"})
# ArrowAI.pprint(prediction3)

# model4 = ArrowModel.createModel(cluster, {"name": "model for centroid 000001", "centroid": "000004"})
# prediction4 = ArrowAI.predict(model4,{"sepal length": 5,"sepal width": 2.5},{"name": "my prediction4"})
# ArrowAI.pprint(prediction4)

# model5 = ArrowModel.createModel(cluster, {"name": "model for centroid 000001", "centroid": "000000"})
# prediction5 = ArrowAI.predict(model5,{"sepal length": 5,"sepal width": 2.5},{"name": "my prediction5"})
# ArrowAI.pprint(prediction5)

# clusterData = ArrowAI.getCluster(cluster)
# ArrowAI.pprint(clusterData['object'])

