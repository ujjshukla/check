from bigml.api import BigML

api = BigML('ujjshukla', 'c8d9a01c7aa902c70a7ce9a98929d07deca14c80')

def getApi():
	return api

def load_data(file):
	source = api.create_source(file)
	return api.create_dataset(source)

def pprint(data):
	api.pprint(data)

def createCluster(dataset, options):
	cluster = api.create_cluster(dataset, options)
	# model = api.create_model(cluster, {"name": "model for centroid 000001", "centroid": "000001"})
	return cluster

def createModel(data, options):
	model = api.create_model(data, options)
	return model
	
def predict(model, newData, options):
	return api.create_prediction(model,newData, options)

def getCluster(cluster):
	cluster = api.get_cluster(cluster)
	return cluster