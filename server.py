import code as cd

from flask import Flask, jsonify, abort, make_response, request
app = Flask(__name__)

@app.route('/train', methods=['GET'])
def train():
	cd.train()
	return jsonify({'done': True})

@app.route('/predict', methods=['GET'])
def predict():
	json_ = request.json
	result = cd.predict(json_)
	return jsonify({'result': result})

if __name__ == '__main__':
	app.run(port=8080)